#!/usr/bin/perl -w
use strict;
use CGI;
use CGI::Carp qw ( fatalsToBrowser );
use File::Basename;
use Encode 'decode_utf8';
binmode STDOUT, ":utf8";
binmode STDIN, ":utf8";
$ENV{'PATH'} = '/bin:/usr/bin:/usr/local/bin';

$CGI::POST_MAX = 1024 * 10000;
my $safe_filename_characters = "a-zA-Z0-9_.-";
my $upload_dir = "/tmp";

my $query = new CGI;
my $filename = $query->param("docxUpload");

if ( !$filename )
{
    print $query->header();
    print "There was a problem uploading your Docx file (try a smaller file).";
    exit;
}

my ( $name, $path, $extension ) = fileparse ( $filename, '..*' );
$filename = $name . $extension;
$filename =~ tr/ /_/;
$filename =~ s/[^$safe_filename_characters]//g;

if ( $filename =~ /^([$safe_filename_characters]+)$/ )
{
    $filename = $1;
}
else
{
    die "Filename contains invalid characters";
}

my $upload_filehandle = $query->upload("docxUpload");

open ( UPLOADFILE, ">$upload_dir/$filename" ) or die "$!";
binmode UPLOADFILE;

while ( <$upload_filehandle> )
{
    print UPLOADFILE;
}

close UPLOADFILE;

my $skip = "";
my $sep = "";
my @skips = ("ucp-cleanup","editoria-reduce","mml2tex");

foreach my $sk (@skips) {
    if($query->param($sk)) {
	$skip .= $sep . $sk;
	$sep = ",";
    }
}

my $branch = $query->param("branch");
my $XSL = "XSweet/applications/PIPELINE.xsl";
my $XSLwax2 = "XSweet-wax2/XSweet/applications/PIPELINE.xsl";
my $XSLsplit = "docx-splitter/XSweet/applications/docx-splitter/split-for-xsweet.xsl";
if($branch eq 'wax2') {
    $XSL = $XSLwax2;
}
if($branch eq 'docx-splitter') {
    $XSL = $XSLsplit;
}

my $mdf = `md5sum -b $upload_dir/$filename|grep -oP "^[^ ]+"`;
chomp($mdf);
my $zip_dir = "/tmp/${mdf}_${filename}";
`echo "md5:${zip_dir}"`;
$zip_dir=~s/[.]docx//i;
`mkdir $zip_dir`;
`unzip -d $zip_dir $upload_dir/$filename`;
`rm -f /var/www/html/images/*.*`;
`ln -s $zip_dir/word/media/*.* /var/www/html/images/`;
`ln -s $zip_dir/media/*.* /var/www/html/images/`;
my $out=decode_utf8(`java -jar XSweet-wax2/XSweet/lib/SaxonHE10-3J/saxon-he-10.3.jar -xsl:$XSL skip=$skip -s:$zip_dir/word/document.xml`);

$out=~s/(<img[^<>]+src=["']?)[^"]+[\/]([^\/]+[.](?:jpe?g|png|gif|svg|tiff?))/$1\/images\/$2/gi;
print $query->header("text/html;charset=UTF-8");
if($branch eq 'docx-splitter') {
    my $html = "";
    my @counts = $out =~/<file>document[0-9]+[.]xml/g;
    my $n = scalar @counts;
    print "Splitting into " . $n . " chapters ...";
    foreach ( @counts ) {
	my $foo = $_;
	$foo=~s/<file>//;
	my $htm = decode_utf8(`java -jar XSweet-wax2/XSweet/lib/SaxonHE10-3J/saxon-he-10.3.jar -xsl:$XSLwax2  skip=$skip -s:$zip_dir/word/$foo`);
	$htm=~s/<\/body>.*$/<div id='docx_split_${foo}' class='docx_split_rule'><b class='scissor'>\&#x2702;<\/b><hr width='100%' \/><\/div>/s;
	$foo=~s/document([0-9]+)[.]xml/$1/;
	$htm=~s/id=\"main\"/id="main${foo}"/;
	if($html ne "") {
	    $htm=~s/^.*<body[^<>]*>//s;
	}
	$html .= $htm;
    }
    $out = $html;
}
else {
    #$out=~s/(<body[^<>]*>)/$1<br>branch:$branch<br>skip:$skip<br>/;
}
print $out;
