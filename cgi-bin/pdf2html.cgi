#!/usr/bin/perl -w

use strict;
use CGI;
use CGI::Carp qw ( fatalsToBrowser );
use File::Basename;
use Encode 'decode_utf8';
binmode STDOUT, ":utf8";
binmode STDIN, ":utf8";
$ENV{'PATH'} = '/bin:/usr/bin:/usr/local/bin';

$CGI::POST_MAX = 1024 * 5000;
my $safe_filename_characters = "a-zA-Z0-9_.-";
my $upload_dir = "/tmp";

my $query = new CGI;
my $filename = $query->param("PDFUpload");
my $img = $query->param("skip-image");

if ( !$filename )
{
print $query->header();
print "There was a problem uploading your PDF (try a smaller file).";
exit;
}

my ( $name, $path, $extension ) = fileparse ( $filename, '..*' );
$filename = $name . $extension;
$filename =~ tr/ /_/;
$filename =~ s/[^$safe_filename_characters]//g;

if ( $filename =~ /^([$safe_filename_characters]+)$/ )
{
$filename = $1;
}
else
{
die "Filename contains invalid characters";
}

my $upload_filehandle = $query->upload("PDFUpload");

open ( UPLOADFILE, ">$upload_dir/$filename" ) or die "$!";
binmode UPLOADFILE;

while ( <$upload_filehandle> )
{
print UPLOADFILE;
}

close UPLOADFILE;

my $mdf = `md5sum -b $upload_dir/$filename|grep -oP "^[^ ]+"`;
chomp($mdf);
my $xml_dir= $mdf;
`mkdir $upload_dir/$xml_dir`;
my $fmg = "";
if($img) {
    $fmg = "-noImage";
}
`./pdfalto/pdfalto $fmg -fullFontName $upload_dir/$filename $upload_dir/$xml_dir/doc.xml`;
my $out=decode_utf8(`java -jar XSweet/lib/SaxonHE9-9-1-1J/saxon9he.jar -xsl:XSweet/applications/pdf2html/PIPELINE.xsl -s:$upload_dir/$xml_dir/doc.xml`);

`rm -f /var/www/html/images/*.*`;
`ln -s $upload_dir/$xml_dir/doc.xml_data/*.* /var/www/html/images/`;

$out=~s/(<img[^<>]+src=["']?)[^"]+[\/]([^\/]+[.](?:jpe?g|png|gif|tiff?))/$1\/images\/$2/gi;

print $query->header("text/html;charset=UTF-8");
print $out;
